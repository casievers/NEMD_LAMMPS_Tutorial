#!/bin/bash

# These are the input parameters that the script takes
# $1 == File name
# $2 == number of bins (amount of times you binned your device in the heat transport direction)
# $3 == number of frames (this is the number of temperature profile snapshots you want to use)
# $4 == length of device (EX: If you have a 50nm length in your heat transport direction type 50)
# $5 == Name of outfile

# This script always takes the last n frames

awk 'NR > 4 {print}' $1 | \
awk -v n=$(($2+1)) 'NR % n !=0' | \
awk -v l=$4 '{print $2*l,"\t"$4}' | \
awk -v n=$2 '1; NR % n == 0 {print " "}' | \
sed '/^\s*$/d' | \
tail -$(($2*$3)) | \
awk '{if ($2 != 0) {sum[$1]=sum[$1] + $2; nr[$1]++; a[NR]=$1; b[NR]=$2}}
END {for (i in sum) {avg[i] = sum[i]/nr[i]}} 
END {for (i in avg) {print i, avg[i] }}' | \
sort -n | \
sort -n > $5

#awk 'NR % 21 !=0' $1 | awk -v l=$3 '{print $2*l,"\t"$4}' | awk -v n=20 '1; NR % n == 0 {print " "}' | sed '/^\s*$/d' | tail -$2 | \
#awk '{sum[$1]=sum[$1] + $2; nr[$1]++; a[NR]=$1; b[NR]=$2} END {for (i in sum) {avg[i] = sum[i]/nr[i]}} END {for (i in a) {print a[i], b[i], avg[a[i]] }}' | \
#sort -n | awk '{std[$1]=std[$1] + ($3 - $2)^2; nr[$1]++; avg[$1]=$3} END {for (a in std) {print a, avg[a], std[a]^0.5/(nr[a]-1)/nr[a]^0.5}}' | sort -n > temperature_gradient.dat

#awk 'NR % 21 !=0' $1 | awk -v l=$3 '{print $2*l,"\t"$4*1.6*10^-19/(1.38*10^-23)}' | awk -v n=20 '1; NR % n == 0 {print " "}' | sed '/^\s*$/d' | tail -$2 | awk '{sum[$1]=sum[$1] + $2; nr[$1]++; a[NR]=$1; b[NR]=$2} END {for (i in sum) {avg[i] = sum[i]/nr[i]}} END {for (i in a) {print a[i], b[i], avg[a[i]] }}' | sort -n | awk '{std[$1]=std[$1] + ($3 - $2)^2; nr[$1]++; avg[$1]=$3} END {for (a in std) {print a, avg[a], std[a]^0.5/(nr[a]-1)/nr[a]^0.5}}' | sort -n > temperature_gradient.dat
