###############################mm
# Atom style - charge/vdw/bonded#
#################################
atom_style full

##############################################
#Units Metal : eV       - ps - angstrom - bar#
#      Real  : kcal/mol - fs - angstrom - atm#
##############################################
units   	metal

############
#Run number#
############
variable run_no   equal   2             # is it a restart? 
variable res_no   equal   ${run_no}-1   # restart file number 

#######################################
#Random Seeds and Domain Decomposition#
#######################################
variable iseed0 equal 2357  
variable iseed1 equal 26488 
variable iseed2 equal 10669 
processors      * * *

###########
#Data File#
###########
variable inpfile   string   silicon_input_file.lmp
variable resfile   string   final_restart.${res_no}
variable ff_file   string   ff-silicon.lmp

##########
#Run Type#
##########
variable minimise equal    0   #Energy Minimization
variable md       equal    1   #Equilibrium Molecular Dynamics
variable nemd     equal    0   #Non-Equilibrium Molecular Dynamics

###############################
#Molecular Dynamics Parameters#
###############################
variable ens      equal        5       # ensemble (0=nve, 1=nvt, 2=npt, 3=ber, 4=lang, 5=stoc, 6=vres, 7=stoch)
variable ts       equal        0.0005  # simulation timestep (time units)
variable nequil   equal        0       # number of equilibration steps
variable nsteps   equal    20000       # number of MD steps 

variable temp_s   equal      300       # starting temperature 
variable temp_f   equal      300       # final simulation temperature 
variable trel     equal        1       # thermostat relaxation time
variable tscale   equal        1       # thermostat relaxation freq - vel rescaling only
variable deltat   equal       10       # maximum temperature change - vel rescaling only

variable npttype  string       iso     # type of NPT (iso, aniso, tri, z...)
variable pres     equal        1.01325 # pressure (NPT runs only)
variable prel     equal        1.0     # barostat relaxation time

neighbor 1 bin

###################
#Output Parameters#
###################
variable ntraj    equal     1000       # trajectory output frequency - all system
variable ntraj_s  equal     -100       # trajectory output frequency - solute only
variable nthermo  equal      200       # thermodynamic data output frequency 
variable dbg_erg  equal       -1       # print out the energy in a gulp friendly mode for debugging

################################
#Energy Minimization Parameters#
################################
variable mtraj    equal        1       # trajectory output frequency - all system
variable etol     equal     1e-4       # % change in energy
variable ftol     equal     1e-4       # max force threshold (force units)
variable maxiter  equal     1000       # max # of iterations

########################
#3D Periodic Simulation#
########################
boundary 	p p p

#############################
#Reading the input structure#
#############################
if "${run_no} == 0" then "read_data ${inpfile}" else "read_restart ${resfile}"

#############
#Force Field#
#############
include ${ff_file}

#########
#Regions#
#########
region left   block  0 54.31  0 54.31    0     21.724 
region right  block  0 54.31  0 54.31  108.62 130.344 
region system block  0 54.31  0 54.31    0    217.24

########
#Groups#
########
if "${run_no} == 0" then "group left    region left" &
	"group right   region right" &
	"group system  region system"

######################
#Thermodynamic Output#
######################
variable str_basic string 'step time pe temp press'

#MD ensemble (0=nve, 1=nvt, 2=npt, 3=ber, 4=lang, 5=stoc, 6=vres)
variable str_ens string ' '
if "${ens} == 0" then "variable str_ens string 'etotal'"
if "${ens} == 2" then "variable str_ens string 'vol pxx pyy pzz cella cellb cellc cellakpha cellbeta cellgamma'"

#Variable for a gulp friend output
variable str_dgb string ' '
if "${dbg_erg} == 1" then &
  "variable e2body    equal ebond+evdwl" &
  "variable ecoul_tot equal ecoul+elong" &
  "variable str_dbg string 'ebond eangle edihed eimp evdwl ecoul elong etail v_e2body v_ecoul_tot'"

if "${ens} >= 0" then "thermo_style custom time temp  pe  etotal press vol  cpu" &
	"thermo ${nthermo}" &
	"thermo_modify flush yes" 

#####################
#Energy Minimization#
#####################
if "${minimise} <= 0 || ${run_no} > 0" then "jump SELF end_minimise"
  print "Doing CG minimisation"
  dump mdcd all dcd ${mtraj} min.dcd
  dump_modify mdcd unwrap yes
  min_style cg
  min_modify line quadratic
  minimize ${etol} ${ftol} ${maxiter} ${maxiter}
  reset_timestep 0
  undump mdcd
label end_minimise

################
#Timestep in ps#
################
timestep ${ts}

##############
#Restart file#
##############
restart 100000 restart.1 restart.2

###################
#Trajectory output#
###################
#dump  xyz all atom 1000 silicon.lammpstrj

if "${ntraj} > 0" then &
  "dump 1 all dcd ${ntraj} trajectory.${run_no}.dcd" &
  "dump_modify 1 unwrap yes"

#####################
#Langevin Thermostat#
#####################
if "${nemd} > 0" then "compute ke all ke/atom" &
        "variable temp atom c_ke/1.5" &
        "compute Thot  left  temp/region system" &
        "compute Tcold right temp/region system" &
        "fix nve all nve" &
        "fix hot  left  langevin 350 350 1.00 48279 tally yes" &
        "fix cold right langevin 250 250 1.00 48279 tally yes" &
        "fix_modify hot  temp Thot" &
        "fix_modify cold temp Tcold" &
        "variable tdiff equal c_Thot-c_Tcold" &
        "fix ave all ave/time 10 100 1000 v_tdiff ave running file langevin.ave" &
 	"variable hot_flux  equal  -f_hot/(0.00001+time)" &
        "variable cold_flux equal  f_cold/(0.00001+time)" &
        "thermo_style custom time temp c_Thot c_Tcold v_hot_flux v_cold_flux v_tdiff" &
        "thermo ${nthermo}" &
        "thermo_modify flush yes" &
        "compute layers all chunk/atom bin/1d z lower 0.05 units reduced" &
        "fix 2 all ave/chunk 10 100 1000 layers v_temp file profile.langevin" 

###############################################################
#Ensembles (0=nve,1=nvt, 2=npt, 3=ber, 4=lang, 5=stoc, 6=vres)#
###############################################################
if "${md} > 0" then 'print "Setting up the ensembles"' &
        'if "${run_no} == 0" then "velocity all create ${temp_s} ${iseed0} mom yes dist gaussian"' &
        'if "${ens} == 0" then "fix nve all nve"' &
        'if "${ens} == 1" then "fix nvt all nvt temp ${temp_s} ${temp_f} ${trel} tchain 5"' &
        'if "${ens} == 2" then "fix npt all npt temp ${temp_s} ${temp_f} ${trel} ${npttype} ${pres} ${pres} ${prel} tchain 5 pchain 5 mtk yes"' &
        'if "${ens} == 3" then "fix nve all nve" "fix ber all temp/berendsen ${temp_s} ${temp_f} ${trel}"' &
        'if "${ens} == 4" then "fix nve all nve" "fix lang all langevin ${temp_s} ${temp_f} ${trel} ${iseed1} tally yes zero yes"' &
        'if "${ens} == 5" then "fix nve all nve" "fix stoch all temp/csvr ${temp_s} ${temp_f} ${trel} ${iseed1}"' &
        'if "${ens} == 6" then "fix nve all nve" "fix stoch all temp/csld ${temp_s} ${temp_f} ${trel} ${iseed1}"' &
        'if "${ens} == 7" then "fix nve all nve" "fix vres all temp/rescale ${tscale} ${temp_s} ${temp_f} ${tmin} ${tmax}"'


if "${md} > 0 || ${nemd} > 0" then "print 'Doing Molecular dynamics'" &
	"run ${nsteps}" &
	"write_restart final_restart.${run_no}" 

