<!--- This is using github markdown format --->

Lammps Non_Equilibrium Molecular Dynamics Bulk Silicon Tutorial
===============================================================

Start off in the tutorial folder (which can also be referred to as the tutorial directory).  

Both of the following are meant to be typed in as commands.  

    echo 'hello world'
``` echo 'hello world' ```

Creating the bulk silicon coord file (creation of silicon_input_file.pdb):
--------------------------------------------------------------------------

Use the terminal with ase installed in your python library.  
Do not include >>> they represent that you are typing commands into a python interpreter.  
Make sure that you are in the tutorial directory.  

    python
    >>>from ase.spacegroup import crystal
    >>>Si = crystal('Si', [(0,0,0)], spacegroup=227, cellpar=[5.431, 5.431, 5.431, 90, 90, 90], size=(10,10,40))
    >>>from ase.io import write
    >>>write('silicon_input_file.pdb', Si, format='proteindatabank')
    
To exit press ctrl-D.  

Creating the system (creation of silicon_input_file.lmp):
---------------------------------------------------------

Make sure you are in the tutorial directory.  
Using GPTA to create a lammps system file called silicon_input_file.lmp.  
I suggest typing `gpta3.x --help --system` for more gpta system creation options.  

    gpta3.x --i silicon_input_file.pdb --system +mol Si +lmp ff-silicon.lmp +o silicon_input_file.lmp

Running the first temperature equilibration (NVT):
--------------------------------------------------

Assuming that you are in the tutorial directory.  
Copy the system input file to nvt folder.  
* cp = copy  

<!--- --->  

    cp silicon_input_file.lmp nvt/  

    
Move yourself into the nvt folder.  
* cd = change directory  

<!--- --->  

    cd nvt/

Run Lammps.  
* nohup = no hold up (helps to keep programs running even if your account gets logged out)  
* mpirun -np 4 lmp_mpi = run lammps using 4 processors with MPI (MPI allows you to run program in parallel and to change the number of processors change 4 to the number of processors you would like)  
* lmp_mpi < in.silicon = passing in.silicon as the input script  
* > out.silicon = write output to out.silicon (holds slightly different information from the log file log.lammps)  
* & = run program in background so you can keep using the terminal (do not use on peloton)  

<!--- --->  

    nohup mpirun -np 4 lmp_mpi < in.silicon > out.silicon &

When your first temperature equilibration is done it is time to start a unit cell relaxation.  

Running the unit cell relaxation (NPT):
---------------------------------------

Assuming you are in the nvt folder.  
Copy final_restart.0 from the nvt folder (while in the nvt folder) to the npt folder.  

    cp final_restart.0 ../npt/

Move into the npt folder.  

    cd ../npt/

Run Lammps.  
    
    nohup mpirun -np 4 lmp_mpi < in.silicon > out.silicon &

When your unit cell relaxation is done it is time to start a temperature reequilibration.  

Running the second temperature equilibration (NVT):
---------------------------------------------------

Assuming that you are in the npt folder.  
Copy final_restart.1 from the npt folder to second_nvt folder.  

    cp final_restart.1 ../second_nvt/

Move into the second_nvt folder.  

    cd ../second_nvt/

Run Lammps.  

    nohup mpirun -np 4 lmp_mpi < in.silicon > out.silicon &

When your temperature reequilibration is done it is time to check if your system is well equilibrated.  

Running a equilibration check (NVE):
------------------------------------

Assuming you are in the second_nvt folder.  
Copy final_restart.2 from the second_nvt folder to the nve folder.  

    cp final_restart.2 ../nve/

Move into the nve folder.  

    cd ../nve/

Run Lammps.  

    nohup mpirun -np 4 lmp_mpi < in.silicon > out.silicon &

When your nve simulation is done, gather your energy vs time data.  

    awk 'FNR > 28 {print $1, $4}' out.silicon > energy.dat

Now that the energy vs time is in the file energy.dat, plot it with xmgrace.  

    xmgrace energy.dat

If the energy is converged it is time to run the NEMD simulation.  

Running Non-Equilibrium Molecular Dynamics:
-------------------------------------------

Assuming you are in the nve folder.  
Copy final_restart.2 (not final_restart.3) from the nve folder to the NEMD folder.  

    cp final_restart.2 ../NEMD/

Move into the NEMD folder.  

    cd ../NEMD/

Run Lammps.  

    nohup mpirun -np 4 lmp_mpi < in.silicon > out.silicon &

When the simulation is finally done, extract the useful quantities.  

    awk 'FNR > 28 {print $1, $5, $6}' out.silicon > flux.dat

Now that you have the flux, plot the flux.  
The heat bath flux is column 2 and the heat sink bath is column 3.  

    xmgrace -nxy flux.dat

From the flux plot find the average of the flux during the steady state (when flux is converged).  
Use your best judgement when determining where the flux is converged.  
Try zooming into by double clicking the y axis (careful not to double click the plotted data) and setting the range in which to limit the y axis.  
Once you are zoomed into the converged region, try joining the two data arrays using the join operation which can be found by clicking on Data set operations from the Data drop down menu.  
Try taking a running average of the data, this is done by clicking the Running averages option within the Transformations tab of the Data drop down menu.  
When you are in the Running averages menu select the data set, change Restrictions to Inside graph, and divide by one less than the number of data points within the plotting window (may be tedious).  
To view the flux value, choose data sets from Edit drop down menu and then click on the new data set.  
Write down this value divided by two, this is the flux.  

Temperature Profile Script (open and read script header if you wish to use this for a different simulation)  

    bash ../temp_prof.sh profile.langevin 100 5000 21.7 temperature_profile.dat

Viewing the temperature profile

    xmgrace temperature_profile.dat

Zoom in on a linear regime of the plot, i.e. 3 nm to 10 nm.  
Make a linear regression of the data, this is done by clicking the Regression option within the Transfomations tab of the Data drop down menu. (Be sure to set Restrictions to inside graph)  
Write down the slope from this linear regression.  

Take the converged flux values and divide by the slope of the temperature profile.  
Take the flux/slope and divide by 10.862 (the area of the cross section).  
This is the value of the thermal conductivity in eV/(nm*K*ps)  
Multiply by 160 if you want the thermal conductivity in W/(m*K)  

